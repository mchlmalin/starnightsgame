﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDITestGame
{
    public partial class Form2 : Form
    {
        static int winW = 242;
        static int winH = 101;
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.Width = winW;
            this.Height = winH;
            pictureBox1.SendToBack();
            this.SetClientSizeCore(winW, winH);
            this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
            this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != string.Empty)
            {
                Form1.newRecordAdded = true;
                this.Close();
            }
            else
                MessageBox.Show("Введите имя");
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1 f1 = new Form1();
            Form1.Active.Name = textBox1.Text;
            //f1.load_buffer(Form1.cardsInLvl[Form1.level]);
        }

        private void Form2_Move(object sender, EventArgs e)
        {
            Form1 f1 = new Form1();
            f1.load_buffer(Form1.cardsInLvl[Form1.level]);
        }
    }
}
