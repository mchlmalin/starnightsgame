﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDITestGame
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
           
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Form1 mainForm = new Form1();
            //Application.Idle += new EventHandler(mainForm.OnIdle);
            Application.Run(new Form1());
        }

        private static void Application_Idle(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
