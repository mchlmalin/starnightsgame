﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Timers;
using System.Media;
using System.Drawing.Imaging;
using NAudio.Wave;

namespace GDITestGame
{
    enum States { nLevels = 9 };
    enum Difficulty {EASY, NORMAL, HARD};

    public partial class Form1 : Form
    {
        // загрузить статистику c файла
        static public StatisticRecord statiList = new StatisticRecord();
        static public bool newRecordAdded = false;
        static string appPath = Application.StartupPath;
     

        static private WaveOut waveOut = null;
        static private Mp3FileReader mp3reader = null;

        // SoundPlayer sound, sound1, sound2;
        static XmlSerializer formatter = null;
        static public Profile Active = null;
        static String pathP;
        Thread newThread = null;
        static int i, j, m, n, whatCard;
        static bool isGameStarted = false;
        static int fpsForAnime = 0; //0 - no Anime for card,1 - Shining,2 Shining and Anime Open Card
        static int WhichCardAnime = 0;
        static int WhichCardAnimeSecC = 0;
        static int MainAnimeCount = 1;
        static int xAnime = 0, yAnime = 0, NeedCardAn = 0, NeedCardBn = 0, NeedCardCn = 0;
        static int countFPS = 0;
        static int countFPS2 = 0;
        static bool OpenedCard = true;
        static bool SecondCard = false;
        static bool From2To1Card = false;
       
        static Graphics gr_form;
        static Graphics[] gr_buffer = new Graphics[1000];
        static Bitmap[] bmp_diam = new Bitmap[1000];
        static Bitmap[] bmp_anime = new Bitmap[1000];
        static Bitmap bmp_buffer;
        MemoryStream[] playlist;
        // game states
        static int winWidth = 1280, winHeight = 720;
        static Difficulty difficulty = Difficulty.NORMAL; //default difficulty
        static public int level = 0, levelUsed = 0;
        static public List<int> cardsInLvl = new List<int> { 8, 12, 16, 12, 16, 20, 18, 32, 40 };
        couple_cards[] cards = new couple_cards[50];
        static int MissClicks = 0;
        static int[] MissCoin = {20, 40, 80};
        static int[] Schet = { 400, 600, 800, 1000, 1400, 1800, 2000, 2500, 3000 };
        
        // arr for choosing how much will be repeated pars in game and how much cards for level
        static int[] easy = { 1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 2 };
        static int[] normal = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1 };
        // it's depending on level and difficulty
        //
        // if (easy) 
        // int[] mas = {2,2,1,1,1,1 (1_1 level)...
        //
        // if(hard)
        // int[] mas = {all = 1}

        //Height for drawing
        static int heightM = 26;
        //selection part
        static int selectionState = 0;
        static couple_cards sel1 = null, sel2 = null;
        //offset against form
        static int formOffsetX = 18;
        static int formOffsetY = 37;
        //
        static int realImageWidth = 104;
        static int realImageHeight = 135;
        //the space between the cards
        static int spaceX = -10;
        static int spaceY = -5;
        //card picture sizes
        static int imageWidth = 140;
        static int imageHeight = 160;
        //buttons picture size
        static int butWidth = 200;
        static int butHeight = 60;
        //first pos for buttons
        static int xButPos = 190;
        static int yButPos = 300;
        //star size
        static int starXY = 100;
        //star positions
        static int starPosX = 238;
        static int starPosY = -70;
        // position for About and Settings
        static int XYAbSet = 70;
        static int posAbSetX = winWidth - 2 * XYAbSet - 2 * 20;
        static int posAbSetY = winHeight - XYAbSet - 20;
        //mouse move coordinates
        public int mouseX = 0;
        public int mouseY = 0;
        //size of "Difficulty"
        static int DifX = 340;
        static int DifY = 80;
        //position for Es Norm Hard
        static int posDESHX = 100;
        static int posDESHY = DifY + 20;
        //size for Easy and Hard
        static int DEHX = 160;
        static int DEHY = 40;
        //size for Normal
        static int DNX = 220;
        static int DNY = 40;
        //pos for but Menu in Levels
        static int posMenuX = 70;
        static int posMenuY = winHeight - butHeight - 40;
        //pos for Pause buttons
        static int posPauseX = 554;
        static int posPauseY = 110;
        //pos for Music Volume
        static int posVolumeX = 610;
        static int posVolumeY = 178;
        //size of Volume Buttons
        static int VolXY = 28;
        //size of (+/-) Music Buttons
        static int PlusMinVolX = 50;
        static int PlusMinVolY = 40;
        //size of game is Saved/Loaded
        static int GameSLX = 380;
        static int GameSLY = 240;
        //Position of game is Saved/Loaded
        static int PosGameSLX = 460;
        static int PosGameSLY = 210;
        //Position of Button OK
        static int PosOKX = PosGameSLX + 114;
        static int PosOKY = PosGameSLY + 145;
        //Size of Button OK
        static int OKX = butWidth - 50;
        static int OKY = butHeight;
        //Size of window "Stage Cleared"
        static int StgClrX = 380;
        static int StgClrY = 220;
        //Position of window "Stage Cleared"
        static int StgClrPosX = 458;
        static int StgClrPosY = 220;
        //to make numbers of score be in center
        static int moveNumber = 0;
        //Count for slide eff
        public int SlideCount = 0; // max 16 
        //how far move arrow 
        public int SlideMoveX = 10;
        //Position for Profile 
        static int ProfPosX = 440;
        static int ProfPosY = 10;
        //Size of Profile
        static int ProfX = 600;
        static int ProfY = 700;
        //Size of window "AboutUs"
        static int AboutUsX = 0;
        static int AboutUsY = 0;
        //Position of button for window "AboutUs"
        static int PosSkullAboutUsX = 1180;
        static int PosSkullAboutUsY = 635;
        //Size of back for Slide Eff.
        static int SlideEffBackX = 417/2;
        static int SlideEffBackY = 212/2;

        Thread listenerThread;
        /////////
        public static int Volume = 5;
        public static int SavedVol = 0;

        //align
        static List<float> alignInLvl = new List<float> {3.5f, 5, 9.5f, 3.5f, 5, 15, 30, 9, 80};
        /////////////////////////////////////////lvl 0  1  2  3  4  5  6  7  8

        public bool pause = false;
        public bool music = false;
        public bool muted = false;
        public bool Saved = false;
        public bool Loaded = false;
        public bool Profile = false;
        public bool LoadingGame = false;
        public bool AboutUs = false;
        static MemoryStream menuMusic;
        static MemoryStream mp3file1;
        static MemoryStream mp3file2;
        static MemoryStream mp3file3;
        public Form1()
        {
            InitializeComponent();
         
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // sound = new SoundPlayer(Properties.Resources.sound);
            // sound1 = new SoundPlayer(Properties.Resources.sound1);
            // sound2 = new SoundPlayer(Properties.Resources.sound2);
            menuMusic = new MemoryStream(Properties.Resources._03_Never_Departing_Shadow);
            mp3file1 = new MemoryStream(Properties.Resources._02_Footprints);
            mp3file2 = new MemoryStream(Properties.Resources._04_Forward_We_Move_Upward_We_Climb);
            mp3file3 = new MemoryStream(Properties.Resources._05_Sky_Wind_and_Waves);
            playlist = new MemoryStream[] { mp3file1, mp3file2, mp3file3 };
            //playlist = new SoundPlayer[] {sound, sound1, sound2};
            //this.DoubleBuffered = true;
            this.Width = winWidth;
            this.Height = winHeight;
            playMusic(menuMusic);
            
            this.SetClientSizeCore(winWidth, winHeight);
            this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
            this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
            #region GraphicSets
            gr_form = this.CreateGraphics();
            gr_form.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Low;
            gr_form.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            gr_form.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
            gr_form.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighSpeed;
            //g_form.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;

            bmp_buffer = new Bitmap(winWidth, winHeight, gr_form);
            for (i = 0; i < 125; i++)
            {
                gr_buffer[i] = Graphics.FromImage(bmp_buffer);
                gr_buffer[i] = Graphics.FromImage(bmp_buffer);
                gr_buffer[i].InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Low;
                gr_buffer[i].SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
                //g_backbuff.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;

                gr_buffer[i].CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
                gr_buffer[i].PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighSpeed;
            }
            #endregion
            int[] arr = { 0, 0, 0, 0, 0 };
            Active = new Profile("Anon",0, arr); // 0..8
            //ProList = new List<Profile>();
            //ProList.Add(Active);

            formatter = new XmlSerializer(typeof(Profile));
            pathP = Environment.GetEnvironmentVariable("USERPROFILE");

            // Menu
            bmp_diam[303] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Menu"), new Size(winWidth, winHeight));
            bmp_diam[304] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BNewGame"), new Size(butWidth, butHeight));
            bmp_diam[305] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("ANewGame"), new Size(butWidth, butHeight)); 
            bmp_diam[306] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BLoad"), new Size(butWidth, butHeight));
            bmp_diam[307] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("ALoad"), new Size(butWidth, butHeight));
            bmp_diam[308] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BLevels"), new Size(butWidth, butHeight));
            bmp_diam[309] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("ALevels"), new Size(butWidth, butHeight));
            bmp_diam[310] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BExit"), new Size(butWidth, butHeight));
            bmp_diam[311] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("AExit"), new Size(butWidth, butHeight));
            bmp_diam[314] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("LevelsMenu"), new Size(winWidth, winHeight));
            // stars for levels 
            bmp_diam[316] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Blvl1"), new Size(starXY, starXY));
            bmp_diam[317] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Alvl1"), new Size(starXY, starXY));
            bmp_diam[318] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Blvl2"), new Size(starXY, starXY));
            bmp_diam[319] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Alvl2"), new Size(starXY, starXY));
            bmp_diam[320] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Blvl3"), new Size(starXY, starXY));
            bmp_diam[321] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Alvl3"), new Size(starXY, starXY));
            bmp_diam[322] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Blvl4"), new Size(starXY, starXY));
            bmp_diam[323] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Alvl4"), new Size(starXY, starXY));
            bmp_diam[324] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Blvl5"), new Size(starXY, starXY));
            bmp_diam[325] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Alvl5"), new Size(starXY, starXY));
            bmp_diam[326] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Blvl6"), new Size(starXY, starXY));
            bmp_diam[327] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Alvl6"), new Size(starXY, starXY));
            bmp_diam[328] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Blvl7"), new Size(starXY, starXY));
            bmp_diam[329] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Alvl7"), new Size(starXY, starXY));
            bmp_diam[330] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Blvl8"), new Size(starXY, starXY));
            bmp_diam[331] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Alvl8"), new Size(starXY, starXY));
            bmp_diam[332] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Blvl9"), new Size(starXY, starXY));
            bmp_diam[333] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Alvl9"), new Size(starXY, starXY));
            
            bmp_diam[335] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Difficulty"), new Size(DifX, DifY));
            bmp_diam[336] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("DEasy"), new Size(DEHX, DEHY));
            bmp_diam[337] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("DAEasy"), new Size(DEHX, DEHY));
            bmp_diam[338] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("DNormal"), new Size(DNX, DNY));
            bmp_diam[339] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("DANormal"), new Size(DNX, DNY));
            bmp_diam[340] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("DHard"), new Size(DEHX, DEHY));
            bmp_diam[341] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("DAHard"), new Size(DEHX, DEHY));
            
            bmp_diam[350] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BMenu"), new Size(butWidth, butHeight));
            bmp_diam[351] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("AMenu"), new Size(butWidth, butHeight));
            
            bmp_diam[352] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BOptions"), new Size(XYAbSet, XYAbSet));
            bmp_diam[353] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("AOptions"), new Size(XYAbSet, XYAbSet));
            bmp_diam[354] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BAbout"), new Size(XYAbSet, XYAbSet));
            bmp_diam[355] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("AAbout"), new Size(XYAbSet, XYAbSet));

            bmp_diam[360] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Pause"), new Size(504, 550));
            bmp_diam[362] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BSave"), new Size(butWidth, butHeight));
            bmp_diam[363] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("ASave"), new Size(butWidth, butHeight));

            // Settings of Music
            bmp_diam[364] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("SettingMusic"), new Size(550, 380));
            bmp_diam[366] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BMusic"), new Size(VolXY, VolXY));
            bmp_diam[367] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("AMusic"), new Size(VolXY, VolXY));
            bmp_diam[368] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BTurnMusicL"), new Size(PlusMinVolX, PlusMinVolY));
            bmp_diam[369] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("ATurnMusicL"), new Size(PlusMinVolX, PlusMinVolY));
            bmp_diam[370] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BTurnMusicR"), new Size(PlusMinVolX, PlusMinVolY));
            bmp_diam[371] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("ATurnMusicR"), new Size(PlusMinVolX, PlusMinVolY));
            bmp_diam[372] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("AMuted"), new Size(VolXY, VolXY));
            bmp_diam[374] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BBack"), new Size(butWidth, butHeight));
            bmp_diam[375] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("ABack"), new Size(butWidth, butHeight));

            // Game is Saved or Loaded (Has only ok)
            bmp_diam[380] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("GameSaved"), new Size(GameSLX, GameSLY));
            bmp_diam[381] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("GameLoaded"), new Size(GameSLX, GameSLY));
            bmp_diam[382] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BOk"), new Size(OKX, OKY));
            bmp_diam[383] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("AOk"), new Size(OKX, OKY));

            // Stage Cleared
            bmp_diam[385] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("StageCleared"), new Size(StgClrX, StgClrY));
            bmp_diam[386] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BOk"), new Size(OKX - 10, OKY - 10));
            bmp_diam[387] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("AOk"), new Size(OKX - 10, OKY - 10));

            //Profile where u can chack score
            bmp_diam[390] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Profile"), new Size(ProfX, ProfY));

            //Slide eff for "Credits"
            bmp_diam[391] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("SlideR"), new Size(20, PlusMinVolY));
            bmp_diam[392] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("SlideRB"), new Size(20, PlusMinVolY));
            //Slide eff back 
            bmp_diam[393] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("ForSlideEffCredits"), new Size(SlideEffBackX, SlideEffBackY));
            //About creators
            bmp_diam[400] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("AboutUs"), new Size(460, 600));

            //When we run app menu should be first for viewer 
            fpsForAnime = 3;

            listenerThread = new Thread(listenerStart);
            listenerThread.Start();

            //timer2 is used for FPS and showing textures
            timer2.Enabled = true;
        }

        public static void playSound(object stream)
        {
            try
            {
                DisposeWave();
                MemoryStream mp3file = (MemoryStream)stream;
                if (mp3file != menuMusic) mp3file.Position = 0;
                mp3reader = new Mp3FileReader(mp3file);
                waveOut = new WaveOut();
                waveOut.Init(mp3reader);
                setOutputVolume(Volume);
                waveOut.Play();
            } catch (Exception)
            {
                DisposeWave();
                menuMusic.Position = 0;

            }
        }

        void listenerStart()
        {
            int oldFps = fpsForAnime;
            bool changed = false;
            while (true)
            {
                if (oldFps != fpsForAnime)
                {
                    oldFps = fpsForAnime;
                    changed = true;

                }
                if (changed && fpsForAnime == 3)
                {
                    playMusic(menuMusic);
                    changed = false;
                    Thread.Sleep(1000);
                }
            }
        }

        private static void DisposeWave()

        {
            if (waveOut != null)
            {
                if (waveOut.PlaybackState == NAudio.Wave.PlaybackState.Playing) waveOut.Stop();
                waveOut.Dispose();
                waveOut = null;
            }
            if (mp3reader != null)
            {
                mp3reader.Dispose();
                mp3reader = null;
            }
        }


        // checking game for active
        private bool AppStilIdle
       {
          get
           {
               NativeMethods.Message msg;
               return !NativeMethods.PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
           }
       }

        #region MouseClick
        // checking cards at first and then at second time. If they are equales then turn off them
        // 
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            // Taking coordinates from mouse
            int eX = e.X - formOffsetX;
            int eY = e.Y - formOffsetY;
            // Check if player is playing game
            if (isGameStarted)
            {
                try
                {
                    // If game is finished u need to click on "OK" in game to start a new one
                    if(fpsForAnime == 5)
                        if ((StgClrPosX + 120 - 4) < mouseX && (StgClrPosX + 120 + OKX - 10 - 5) > mouseX && (StgClrPosY + 150 - 32) < mouseY && (StgClrPosY + 150 + OKY - 40) > mouseY)
                        {
                            DisposeWave();
                            startNewGame();
                        }

                    OpenedCard = true;
                    //-1 means the delay of the inspection
                    if (selectionState == -1) return;
                    //0 -- there are no selected cards
                    if (selectionState == 0)
                    {
                        for (int i = 0; i < cardsInLvl[levelUsed]; i++)
                            if ((cards[i].x < eX && eX < cards[i].x + realImageWidth)
                            && (cards[i].y < eY && eY < cards[i].y + realImageHeight)
                            && (cards[i].isSelected == false) && (cards[i].inGame == true) && (timer1.Enabled == false))
                            {
                                cards[i].inGame = false;
                                fpsForAnime = 2;
                                whatCard = 1;
                                sel1 = cards[i];
                                cards[i].isSelected = true;
                                cards[i].turnedUp = true;
                                NeedCardBn = i;
                                NeedCardAn = i;
                                WhichCardAnime = (cards[i].m + 1)*5;
                                xAnime = cards[i].i;
                                yAnime = cards[i].j;
                                selectionState = 1;
                            }
                            else if ((cards[i].x < eX && eX < cards[i].x + realImageWidth)
                            && (cards[i].y < eY && eY < cards[i].y + realImageHeight)
                            && (cards[i].isSelected == false) && (cards[i].inGame == true) && cards[i] != sel1 && cards[i] != sel2 && (timer1.Enabled == true))
                            {
                                timer1.Enabled = false;
                                if (whatCard == 3)
                                {
                                    sel1.inGame = sel2.inGame = false;
                                    sel1 = sel2 = null;
                                    buff_to_draw();
                                }
                                else
                                {
                                    MissClicks++;
                                    From2To1Card = true;
                                    cards[NeedCardAn].inGame = false;
                                    cards[NeedCardBn].inGame = false;
                                    fpsForAnime = 1;
                                    SecondCard = true;
                                    NeedCardCn = i;
                                }
                            }
                    }
                    else if (selectionState == 1)
                    {

                        for (int ii = 0; ii < cardsInLvl[levelUsed]; ii++)
                            if ((cards[ii].x < eX && eX < cards[ii].x + realImageWidth)
                            && (cards[ii].y < eY && eY < cards[ii].y + realImageHeight)
                            && (cards[ii].isSelected == false) && (cards[ii].inGame == true))
                            {
                                cards[ii].inGame = false;
                                fpsForAnime = 2;
                                timer1.Enabled = false;
                                whatCard = 2;
                                sel2 = cards[ii];
                                cards[ii].isSelected = true;
                                cards[ii].turnedUp = true;
                                NeedCardAn = ii;
                                WhichCardAnime = (cards[ii].m + 1) * 5;
                                xAnime = cards[ii].i;
                                yAnime = cards[ii].j;
                                if (sel1.id == sel2.id)
                                {
                                    whatCard = 3;
                                }
                                buff_to_draw();
                                sel1.isSelected = sel2.isSelected = false;
                                selectionState = 0;
                            }
                    }
                }
                catch
                {
                    selectionState = -1;
                    foreach (couple_cards card in cards)
                        if (card != null)
                        {
                            card.inGame = true;
                            card.isSelected = false;
                        }
                    sel1 = sel2 = null;
                    selectionState = 0;

                    MessageBox.Show("Sorry. There was an error.");
                    return;
                }
            }
            else
            {
                // Check where u click in Menu
                if (fpsForAnime == 3)
                {
                    // if u click on "Load" then u will see window of "Game is loaded" here will be "OK"
                    // This works when u click "OK" to close window of "Game is loaded"
                    if(Loaded)
                        if ((PosOKX - 4) < mouseX && (PosOKX + OKX - 4) > mouseX && (PosOKY - 32) < mouseY && (PosOKY + OKY - 28) > mouseY)
                            Loaded = false;
                    // Check where u click in Menu
                    for (int i = 1; i < 5; i++)
                        if (xButPos < mouseX && (xButPos + butWidth-10) > mouseX && (yButPos + 70 * i - heightM) < mouseY && (yButPos + butHeight + 70 * i - heightM) > mouseY)
                            switch (i)
                            {
                                // Button "New Game"
                                case 1:
                                    AboutUs = false;
                                    music = false;
                                    Profile = false;
                                    Loaded = false;
                                    DisposeWave();
                                    startNewGame();
                                    break;
                                // Button "Load"
                                case 2:
                                    AboutUs = false;
                                    music = false;
                                    Profile = false;
                                    Loaded = true;
                                    using (FileStream fs = new FileStream(pathP + @"\Documents\profiles.xml", FileMode.OpenOrCreate))
                                    {
                                        Active = (Profile)formatter.Deserialize(fs);
                                    }
                                    break;
                                // Button "Levels"
                                case 3:
                                    AboutUs = false;
                                    music = false;
                                    Profile = false;
                                    Loaded = false;
                                    fpsForAnime = 4;
                                    break;
                                // Button "Exit"
                                case 4:
                                    Application.Exit();
                                    break;
                            }
                    // There's 2 buttons in right down corner 
                    for (int i = 0; i < 2; i++)
                        if (posAbSetX + 74 * i < mouseX && (posAbSetX + XYAbSet + 80 * i-8) > mouseX && (posAbSetY - heightM) < mouseY && (posAbSetY + XYAbSet - heightM) > mouseY)
                            switch (i)
                            {
                                // button "Shell" - Music Setting
                                case 0:
                                    AboutUs = false;
                                    Profile = false;
                                    Loaded = false;
                                    if (music == false)
                                        music = true;
                                    else
                                        music = false;
                                    break;
                                // button "Skull" - Profile
                                case 1:
                                    AboutUs = false;
                                    music = false;
                                    Loaded = false;
                                    if (Profile == false)
                                        Profile = true;
                                    else
                                        Profile = false;
                                    Loaded = false;
                                    break;
                            }
                    if ((PosSkullAboutUsX - 143 - SlideMoveX * SlideCount) < mouseX && (PosSkullAboutUsX - 155 + PlusMinVolX) > mouseX && (PosSkullAboutUsY - 12) < mouseY && (PosSkullAboutUsY - 20 + PlusMinVolY) > mouseY)
                    {
                        if (!AboutUs)
                        {
                            Profile = false;
                            music = false;
                            Loaded = false;
                            AboutUs = true;
                        }
                        else AboutUs = false;
                    }
                }
                // If u see "Levels" this will work to check which button was clicked
                if (fpsForAnime == 4)
                {
                    // How much levels are avaible for player
                    int CanPlay = Active.LevelIn;
                    int levelNow = 0;
                    // If player click on "Menu"
                    if (posMenuX < mouseX && (posMenuX + butWidth) > mouseX && (posMenuY - 4 - heightM) < mouseY && (posMenuY + butHeight - 4 - heightM) > mouseY)
                    {
                        levelUsed = 0;
                        AboutUs = false;
                        isGameStarted = false;
                        pause = false;
                        fpsForAnime = 3;
                    }
                    // U can choose what difficult will be 
                    for (int i = 1; i < 4; i++)
                    {
                        if ((i == 1 || i == 3) && posDESHX < mouseX && (posDESHX + DEHX) > mouseX && (posDESHY + 85 * i - heightM) < mouseY && (posDESHY + DEHY + 86 * i - heightM) > mouseY)
                        {
                            if (i == 1)
                                difficulty = Difficulty.EASY;
                            if (i==3)
                                difficulty = Difficulty.HARD;
                        } 
                        if (i == 2 && posDESHX < mouseX && (posDESHX + DEHX) > mouseX && (posDESHY + 85 * i - heightM) < mouseY && (posDESHY + DEHY + 86 * i - heightM) > mouseY)
                        {
                            difficulty = Difficulty.NORMAL;
                        }
                    }
                    // Click on levels which are avaible for player
                    for (int i = 1; i < 4; i++)
                        for (int j = 1; j < 4; j++)
                        {
                            if (starPosX + 180 * j < mouseX && (starPosX + starXY + 180 * j - 8) > mouseX && (starPosY + 150 * i - heightM) < mouseY && (starPosY + starXY + 150 * i - heightM) > mouseY && CanPlay > -1)
                            {
                                level = levelNow;
                                startNewGame();
                            }
                            levelNow++;
                            CanPlay--;
                        }
                }
                // if u click on button "Shell" it will let u to work with buttons in "Music settings"
                if (music)
                {
                    // To adjust the volume 
                    for (int i = 0; i < 2; i++)
                        if (posVolumeX - 64 + 238 * i < mouseX && (posVolumeX + PlusMinVolX - 64 + 238 * i) > mouseX && (posVolumeY - 12 - heightM) < mouseY && (posVolumeY + PlusMinVolY - 12 - heightM) > mouseY)
                        {
                            if (i == 0 && Volume > 0) Volume--;
                            if (i == 1 && Volume < 5)
                            {
                                muted = false;
                                Volume++;
                            }
                            setOutputVolume(Volume);
                        }
                    // When u click on box to mute or unmute music
                    if (posVolumeX - 79 < mouseX && (posVolumeX + VolXY - 79) > mouseX && (posVolumeY + 74 - heightM) < mouseY && (posVolumeY + VolXY + 74 - heightM) > mouseY)
                    {
                        if (muted)
                        {
                            Volume = SavedVol;
                            muted = false;
                        }
                        else
                        {
                            SavedVol = Volume;
                            Volume = 0;
                            muted = true;
                        }
                        setOutputVolume(Volume);
                    }
                    // If u clicked "back" it will close "Music Settings"
                    if (posVolumeX - 204 < mouseX && (posVolumeX + butWidth - 206) > mouseX && (posVolumeY + 196 - heightM) < mouseY && (posVolumeY + butHeight + 196 - heightM) > mouseY)
                    {
                        // if u r in game now it will draw graphics again 
                        if (fpsForAnime == 0)
                            buff_to_draw();
                        music = false;
                    }
                }
                // if u r playing game now it will let player use buttons of "ESC Menu"
                else if (pause)
                {
                    for (int i = 0; i < 2; i++)
                        if (posPauseX < mouseX && (posPauseX + butWidth - 8) > mouseX && (posPauseY - 4 + (butHeight + 10) * i - heightM) < mouseY && (posPauseY + butHeight - 4 + (butHeight + 10) * i - heightM) > mouseY)
                        {
                            switch (i)
                            {
                                // Button "New Game"
                                case 0:
                                    Saved = false;
                                    pause = false;
                                    DisposeWave();
                                    startNewGame();
                                    break;
                                // Button "Save game"
                                case 1:
                                    AddStats();
                                    using (FileStream fs = new FileStream(pathP + @"\Documents\profiles.xml", FileMode.Create))
                                    {
                                        formatter.Serialize(fs, Active);
                                    }
                                    Saved = true;
                                    break;
                            }
                        }
                    
                    // If u see window "Game is saved" then u can't click on button "Menu"
                    if (!Saved)
                    {
                        if (posPauseX < mouseX && (posPauseX + butWidth - 8) > mouseX && (posPauseY - 4 + (butHeight + 10) * 2 - heightM) < mouseY && (posPauseY + butHeight - 4 + (butHeight + 10) * 2 - heightM) > mouseY)
                        {
                            // Need to Dispose memory
                            Saved = false;
                            DisposeWave();
                            levelUsed = 0;
                            buff_to_draw();
                            fpsForAnime = 3;
                            isGameStarted = false;
                            pause = false;
                        }

                        if (619 - 4 < mouseX && (619 + XYAbSet - 4) > mouseX && (350 - 4 - heightM) < mouseY && (350 + XYAbSet - 4 - heightM) > mouseY)
                        {
                            Saved = false;
                            music = true;
                        }
                    }

                    // Let u click "OK" to close window "Game is Saved"
                    if (Saved)
                        if ((PosOKX - 4) < mouseX && (PosOKX + OKX - 4) > mouseX && (PosOKY - 32) < mouseY && (PosOKY + OKY - 28) > mouseY)
                            Saved = false;

                }
            }
        }


        //    TIMER for closing cards by time
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.MouseClick -= Form1_MouseClick;
            switch (whatCard)
            {
                case 1:
                    cards[NeedCardAn].inGame = false;
                    buff_to_draw();
                    fpsForAnime = 1;
                    break;
                case 2:
                    cards[NeedCardAn].inGame = false;
                    cards[NeedCardBn].inGame = false;
                    fpsForAnime = 1;
                    SecondCard = true;
                    break;
            }
            timer1.Enabled = false;
            if (whatCard == 3)
            {
                this.MouseClick += Form1_MouseClick;
                cards[NeedCardAn].inGame = false;
                cards[NeedCardBn].inGame = false;
            }
            else MissClicks++;
            buff_to_draw();
            // Check if player won
            int win = 0;
            for (i = 0; i < cardsInLvl[levelUsed]; i++)
            {
                if (cards[i].inGame == true) win++;
            }
            if (win == 0)
            {
                int Sum = Schet[level] - MissClicks * MissCoin[level/3];
                if (Sum < 10) moveNumber = 0;
                else if (Sum < 100) moveNumber = 1;
                else if (Sum < 1000) moveNumber = 2;
                else if (Sum < 10000) moveNumber = 3;
                if (Sum < 0) Sum = 100;
                Active.Score[level] = Sum;
                Active.LevelIn++;
                level++;
                statiList.score[level] = Active.Score[level];
                fpsForAnime = 5;
            }
        }

        public void AddStats()
        {
            Form2 F2 = new Form2();
            //Form2.SetBounds(winWidth / 2, winHeight / 2, 400, 400);
            F2.ShowDialog();
            //TextBox txtBoxName = new TextBox();
            //txtBoxName.Parent = Form2;
        }
        #endregion
            
        // actually no need in this but if you are not lucky guy use this to restart with same difficult
        // Start game depending on rejim, should be normal if player doesn't choose
        public void startNewGame()
        {
            LoadingGame = true;
            fpsForAnime = 0;
            MissClicks = 0;
            timer1.Enabled = false;
            isGameStarted = true;
            
            foreach (couple_cards card in cards)
                if (card != null)
                {
                    card.inGame = true;
                    card.isSelected = false;
                }
            sel1 = sel2 = null;
            selectionState = 0;

            switch (difficulty)
            {
                case Difficulty.EASY:
                    switch (level)
                    {
                        case 0: levelUsed = level; table_rand(4, 8); break;
                        case 1: levelUsed = level; table_rand(5, 12); break;
                        case 2: levelUsed = level; table_rand(7, 16); break;
                        case 3: levelUsed = level; table_rand(5, 12); break;
                        case 4: levelUsed = level; table_rand(7, 16); break;
                        case 5: levelUsed = level; table_rand(9, 20); break;
                        case 6: levelUsed = level; table_rand(8, 18); break;
                        case 7: levelUsed = level; table_rand(13, 32); break;
                        case 8: levelUsed = level; table_rand(16, 40); break;
                    }
                    break;
                case Difficulty.NORMAL:
                    switch (level)
                    {
                        case 0: levelUsed = level; table_rand(4, 8); break;
                        case 1: levelUsed = level; table_rand(6, 12); break;
                        case 2: levelUsed = level; table_rand(7, 16); break;
                        case 3: levelUsed = level; table_rand(6, 12); break;
                        case 4: levelUsed = level; table_rand(7, 16); break;
                        case 5: levelUsed = level; table_rand(9, 20); break;
                        case 6: levelUsed = level; table_rand(8, 18); break;
                        case 7: levelUsed = level; table_rand(14, 32); break;
                        case 8: levelUsed = level; table_rand(18, 40); break;
                    }
                    break;

                case Difficulty.HARD:
                    switch (level)
                    {
                        case 0: levelUsed = level; table_rand(4, 8); break;
                        case 1: levelUsed = level; table_rand(6, 12); break;
                        case 2: levelUsed = level; table_rand(8, 16); break;
                        case 3: levelUsed = level; table_rand(6, 12); break;
                        case 4: levelUsed = level; table_rand(8, 16); break;
                        case 5: levelUsed = level; table_rand(10, 20); break;
                        case 6: levelUsed = level; table_rand(9, 18); break;
                        case 7: levelUsed = level; table_rand(12, 24); break;
                        case 8: levelUsed = level; table_rand(20, 40); break;
                    }
                    break;
            }
        }
        // numForDiffic is used to make sure what we have double couples in game (check int[] easy)
        // numOfCards is used to make life more easier while working this levels
        public void table_rand(int numForDiffic, int numOfCards)
        {
            // id for drawing cards
            List<string> ids = new List<string>()
            { "1","2","3","4","5","6","7","8","9","10", "11","12","13","14","15",
              "16", "17","18", "19","20","21","22","23","24","25"};

            // needed in sort
            couple_cards change = new couple_cards();

            // used to make a rand couple for array of cards
            Random rand = new Random();
            int rand_num;
            String name;

            // it's depending on difficulty, like if count = 3 -> will be 3 same couples in game
            int count = (int)difficulty;
            // for id of array
            int k = 0;
            int max = 24;

            for (i = 0; i < numForDiffic; i++)
            {
                switch (difficulty)
                {
                    case Difficulty.EASY:   count = easy[i];   break;
                    case Difficulty.NORMAL: count = normal[i]; break;
                    case Difficulty.HARD:   count = 1;         break;
                }
                //choosing id for cards and remove id what we used now
                rand_num = (int)rand.Next(0, max);
                name = ids[rand_num] as string;
                ids.RemoveAt(rand_num);
                max--;
                // Checking how much should be couples of this id
                for (j = 0; j < count; j++)
                {
                    cards[k] = new couple_cards();
                    cards[k].id = name;
                    cards[k + 1] = new couple_cards();
                    cards[k + 1].id = name;
                    k += 2;
                }
            }

            // Sort for rand positions
            for (i = numOfCards - 1; i > 0; i--)
            {
                int randInx = rand.Next() % i; // 0 .. arr.Length-2
                change = cards[randInx];
                cards[randInx] = cards[i];
                cards[i] = change;
            }

            // load_buffer(numOfCards);
            bmp_diam[300] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("loading"), new Size(winWidth, winHeight));
            bmp_diam[301] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("loading21"), new Size(winWidth, winHeight));
            bmp_diam[302] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("loading3"), new Size(winWidth, winHeight));

            gr_buffer[53].DrawImage(bmp_diam[300 + level % 3], 0, 0);
            gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
            int curTrack = level % 3;
            playMusic(playlist[curTrack]);
           
            if (backgroundWorker1.IsBusy == false) backgroundWorker1.RunWorkerAsync();
            
        }

        void playMusic(object music)
        {
            newThread = new Thread(Form1.playSound);
            newThread.Start((MemoryStream)music);
        }
        ////////////////////////////////////////////////////////
        public void load_buffer(int numOfCards)
        {
            try
            {
                int countOfAnime = 0;
                //back
                for (int i = 1; i < 6; i++)
                {
                    bmp_anime[countOfAnime] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("_" + 0 + "_" + i.ToString()), new Size(winWidth - 920, winHeight - 559));
                    countOfAnime++;
                }
                for (int j = 0; j < numOfCards; j++)
                {
                    for (int i = 1; i < 6; i++)
                    {
                        bmp_anime[countOfAnime] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("_" + cards[j].id + "_" + i.ToString()), new Size(winWidth - 920, winHeight - 559));
                        countOfAnime++;
                    }
                }
                //backGrounds
                bmp_diam[95] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("back1"), new Size(winWidth, winHeight));
                bmp_diam[96] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("back2"), new Size(winWidth, winHeight));
                bmp_diam[97] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("back3"), new Size(winWidth, winHeight));
                //Heart
                bmp_diam[98] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("Heart"), new Size(imageWidth-25, imageHeight-30));
                //Back of Cards
                bmp_diam[99] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("BackOfCards"), new Size(imageWidth, imageHeight));
                for (i = 0; i < numOfCards; i++)
                {
                    bmp_diam[i] = new Bitmap((Bitmap)Properties.Resources.ResourceManager.GetObject("_" + cards[i].id), new Size(imageWidth, imageHeight)); 
                    //bmp_diam[i].SetResolution(530.0f, 540.0f);
                }
                
                //buff_to_draw();
                //timer2.Enabled = true;
            }
            catch { }
        }


        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);

        [DllImport("User32.dll")]
        private static extern IntPtr GetWindowDC(IntPtr hWnd);

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            listenerThread.Abort();
        }

        public void buff_to_draw()
        {
            
            m = 0;
            n = 0;

            List<int> rows = new List<int>(new int[] { 2, 2, 2, 3, 3, 3, 4, 4, 4 });
            //////////////////////////////////////lvls 0  1  2  3  4  5  6  7  8
            int d = cardsInLvl[levelUsed] / rows[levelUsed];
            List<int> cols = new List<int>(new int[] { d, d, d, d, 6, 9, 9, d, d });
            //////////////////////////////////////lvls 0  1  2  3  4  5  6  7  8  

            for (int i = 0; i < 125; i++)
                gr_buffer[i].Clear(this.BackColor);

            switch (level) {
                case 0:
                case 1:
                case 2: gr_buffer[49].DrawImage(bmp_diam[95], 0, 0); break;
                case 3:
                case 4:
                case 5: gr_buffer[49].DrawImage(bmp_diam[96], 0, 0); break;
                case 6:
                case 7:
                case 8: gr_buffer[49].DrawImage(bmp_diam[97], 0, 0); break;
            }

            for (j = 0; j < rows[levelUsed]; j++)
                for (i = 0; i < cols[levelUsed]; i++)
                {
                    switch (levelUsed)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 7:
                        case 8:
                            draw(i, j); break;
                        case 4:
                            if (!((i == 1 || i == 4) && j == 1))
                                draw(i, j);
                            else draw_heart(i,j);
                            break;
                        case 5:
                            if (!((i == 2 || i == 6) && j == 1) && !((i == 1 || i == 3 || i == 5 || i == 7) && j == 2) && !(i == 4 && j == 0))
                                draw(i, j);
                            else draw_heart(i, j);
                            break;
                        case 6:
                            if (!((i == 0 || i == 2 || i == 4 || i == 5 || i == 7) && j == 0)
                               && !((i == 5 || i == 7) && j == 1)
                               && !((i == 0 || i == 4 || i == 5 || i == 7) && j == 2)
                               && !((i == 0 || i == 1 || (i >= 3 && i <= 6) || i == 8) && j == 3))
                                draw(i, j);
                            break;
                    }
                }
        }

        //make m equal to zero before using [public void draw(int, int)]
        public void draw(int i, int j)
        {
                float x = i * (imageWidth + spaceX - 8) + winWidth / alignInLvl[levelUsed];
                float y = j * (imageHeight + spaceY - 6);
                if (cards[m].inGame == true && cards[m].turnedUp == true)
                  gr_buffer[m].DrawImage(bmp_diam[m], x, y + heightM);
                else if(cards[m].inGame == true && cards[m].turnedUp == false)
                  gr_buffer[m].DrawImage(bmp_diam[99], x, y + heightM);
                cards[m].x = (int)x;
                cards[m].y = (int)y;
                cards[m].i = i;
                cards[m].j = j;
                cards[m].m = m;
                m++;
        }

        public void draw_heart(int i,int j)
        {
            float x = i * (imageWidth + spaceX - 8) + winWidth / alignInLvl[levelUsed] + 13;
            float y = j * (imageHeight + spaceY - 6) + 14;
            gr_buffer[48-n].DrawImage(bmp_diam[98], x, y + heightM);
            n++;
        }

        private void BackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            load_buffer(cardsInLvl[level]);
        }

        private void BackgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LoadingGame = false;
            buff_to_draw();
            timer2.Enabled = true;
        }

        // To know coordinater wherever your mouse is
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            mouseX = e.X - formOffsetX + 12; 
            mouseY = e.Y - formOffsetY + 8; 
        }

        // if u r playing game it lets u to call "ESC menu" by pressing ESC
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                if (fpsForAnime == 0 && !LoadingGame)
                {
                    if (!music)
                    {
                        if (pause == false)
                        {
                            timer1.Enabled = false;
                            pause = true;
                            isGameStarted = false;
                        }
                        else
                        {
                            if (selectionState == 1) timer1.Enabled = true;
                            if (whatCard >= 2) timer1.Enabled = true;
                            pause = false;
                            isGameStarted = true;
                            buff_to_draw();
                        }
                    }
                    else
                    {
                        buff_to_draw();
                        music = false;
                    }
                }
            }
        }

        // to show "Music setting" wherever u want
        private void Music()
        {
                int musVol = Volume;
                gr_buffer[110].DrawImage(bmp_diam[364], 380, 100);
                // Drawing arrows 
                for (int i = 0; i < 2; i++)
                    if (posVolumeX - 64 + 238 * i < mouseX && (posVolumeX + PlusMinVolX - 64 + 238 * i) > mouseX && (posVolumeY - 12 - heightM) < mouseY && (posVolumeY + PlusMinVolY - 12 - heightM) > mouseY)
                        gr_buffer[111 + i].DrawImage(bmp_diam[369 + 2 * i], posVolumeX - 60 + 238 * i, posVolumeY - 8);
                    else
                        gr_buffer[111 + i].DrawImage(bmp_diam[368 + 2 * i], posVolumeX - 60 + 238 * i, posVolumeY - 8);
                // Drawing boxes to show how loud music is now
                for (int i = 0; i < 5; i++)
                {
                    if (musVol > 0)  //&& posVolumeX + 35 * i  < mouseX && (posVolumeX + VolXY + 35 * i) > mouseX && (posVolumeY) < mouseY && (posVolumeY + VolXY) > mouseY)
                        gr_buffer[113 + i].DrawImage(bmp_diam[367], posVolumeX + 35 * i, posVolumeY);
                    else
                        gr_buffer[113 + i].DrawImage(bmp_diam[366], posVolumeX + 35 * i, posVolumeY);
                    musVol--;
                }

                // Drawing if muted or not
                if (!muted)
                    if (posVolumeX - 79 < mouseX && (posVolumeX + VolXY - 79) > mouseX && (posVolumeY + 74 - heightM) < mouseY && (posVolumeY + VolXY + 74 - heightM) > mouseY)
                        gr_buffer[118].DrawImage(bmp_diam[367], posVolumeX - 75, posVolumeY + 78);
                    else
                        gr_buffer[118].DrawImage(bmp_diam[366], posVolumeX - 75, posVolumeY + 78);
                else
                    gr_buffer[118].DrawImage(bmp_diam[372], posVolumeX - 75, posVolumeY + 78);
                // Drawing button "Back"
                if (posVolumeX - 204 < mouseX && (posVolumeX + butWidth - 206) > mouseX && (posVolumeY + 196 - heightM) < mouseY && (posVolumeY + butHeight + 196 - heightM) > mouseY)
                    gr_buffer[119].DrawImage(bmp_diam[375], posVolumeX - 200, posVolumeY + 200);
                else
                    gr_buffer[119].DrawImage(bmp_diam[374], posVolumeX - 200, posVolumeY + 200);
        }
        
            // Used as FPS
            private void timer2_Tick(object sender, EventArgs e)
            {
            float x, y, x1 = 0, y1 = 0;
            switch (fpsForAnime) {
                // No Anime and player is playing now
                case 0:
                    // if player pressed ESC
                    if (pause)
                    {
                            gr_buffer[101].DrawImage(bmp_diam[360], 400, 70);
                        for(int i=0;i<2;i++)
                            if(posPauseX < mouseX && (posPauseX + butWidth - 8) > mouseX && (posPauseY - 4 + (butHeight + 10)*i - heightM) < mouseY && (posPauseY + butHeight - 4 + (butHeight + 10) * i - heightM) > mouseY)
                            gr_buffer[102+i].DrawImage(bmp_diam[305+58*i], posPauseX, posPauseY + (butHeight + 10) * i);
                        else 
                            gr_buffer[102+i].DrawImage(bmp_diam[304+58*i], posPauseX, posPauseY + (butHeight + 10) * i);

                        if (posPauseX < mouseX && (posPauseX + butWidth - 8) > mouseX && (posPauseY - 4 + (butHeight + 10) * 2 - heightM) < mouseY && (posPauseY + butHeight - 4 + (butHeight + 10) * 2 - heightM) > mouseY)
                            gr_buffer[104].DrawImage(bmp_diam[351], posPauseX, posPauseY + (butHeight + 10) * 2);
                        else
                            gr_buffer[104].DrawImage(bmp_diam[350], posPauseX, posPauseY + (butHeight + 10) * 2);

                        if (619 - 4 < mouseX && (619 + XYAbSet - 4) > mouseX && (350-4 - heightM) < mouseY && (350 + XYAbSet-4 - heightM) > mouseY)
                            gr_buffer[105].DrawImage(bmp_diam[353], 619, 350);
                        else
                            gr_buffer[105].DrawImage(bmp_diam[352], 619, 350);
                        // Drawing "Game is saved" if player pressed "SAVE"
                        if (Saved)
                        {
                            gr_buffer[110].DrawImage(bmp_diam[380], PosGameSLX, PosGameSLY);
                            if ((PosOKX - 4) < mouseX && (PosOKX + OKX - 4) > mouseX && (PosOKY - 32) < mouseY && (PosOKY + OKY - 28) > mouseY)
                                gr_buffer[111].DrawImage(bmp_diam[383], PosOKX, PosOKY);
                            else
                                gr_buffer[111].DrawImage(bmp_diam[382], PosOKX, PosOKY);

                        }
                    }
                    // Drawing "Music settings"
                    if(music)
                        Music();
                    // Showing graphics on display
                    gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
                    break;
                // Works to close opened card(s)
                case 1:
                    this.MouseClick -= Form1_MouseClick;
                    OpenedCard = false;
                    MainAnimeCount = 99;
                    fpsForAnime = 2;
                    WhichCardAnime += 4;
                    WhichCardAnimeSecC = (cards[NeedCardBn].m + 1) * 5 + 4;
                    break;
                // Animation of closing or opening card(s)
                case 2:
                    this.MouseClick -= Form1_MouseClick;
                    if (MainAnimeCount <= 10 || MainAnimeCount >=90)
                    {
                        x = (xAnime-1) * (imageWidth  + spaceX - 8) + winWidth / alignInLvl[levelUsed];
                        y = yAnime * (imageHeight  + spaceY - 6) + heightM;
                        if (SecondCard)
                        {
                            x1 = (cards[NeedCardBn].i - 1) * (imageWidth + spaceX - 8) + winWidth / alignInLvl[levelUsed];
                            y1 = cards[NeedCardBn].j * (imageHeight + spaceY - 6) + heightM;
                        }
                        //if (MainAnimeCount <= 4 || (MainAnimeCount <= 95 && MainAnimeCount >= 90)) countFPS = BackCard;
                        if (MainAnimeCount == 0) { countFPS = 0; countFPS2 = 0; }
                        if (MainAnimeCount == 99) { countFPS = WhichCardAnime; countFPS2 = WhichCardAnimeSecC; }

                            if (countFPS == 4 && OpenedCard == true)
                            {
                                buff_to_draw();
                                gr_buffer[50].DrawImage(bmp_anime[countFPS], x, y);
                                gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
                                countFPS++;

                                countFPS = WhichCardAnime;
                                buff_to_draw();
                                gr_buffer[50].DrawImage(bmp_anime[countFPS], x, y);
                                gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
                                countFPS++;
                            }

                            buff_to_draw();
                            gr_buffer[50].DrawImage(bmp_anime[countFPS], x, y);
                            if (SecondCard)
                            {
                                gr_buffer[51].DrawImage(bmp_anime[countFPS2], x1, y1);
                                countFPS2--;
                            }
                            gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
                            if (MainAnimeCount <= 10)
                                countFPS++;
                            else countFPS--;

                            buff_to_draw();
                            gr_buffer[50].DrawImage(bmp_anime[countFPS], x, y);
                            if (SecondCard)
                            {
                                gr_buffer[51].DrawImage(bmp_anime[countFPS2], x1, y1);
                                countFPS2--;
                            }
                            gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
                            if (MainAnimeCount <= 10)
                                countFPS++;
                            else countFPS--;

                            if (MainAnimeCount == 96)
                            {
                                buff_to_draw();
                                gr_buffer[50].DrawImage(bmp_anime[countFPS], x, y);
                                if (SecondCard)
                                {
                                    gr_buffer[51].DrawImage(bmp_anime[countFPS2], x1, y1);
                                    countFPS2--;
                                }
                                gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
                                
                                countFPS = 4;
                                countFPS2 = 4;
                                buff_to_draw();
                                gr_buffer[50].DrawImage(bmp_anime[countFPS], x, y);
                                if (SecondCard) gr_buffer[51].DrawImage(bmp_anime[countFPS], x1, y1);
                                gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
                                countFPS--;
                                countFPS2--;
                            }
                            
                        if (MainAnimeCount <= 10)
                            MainAnimeCount += 3;
                        else  if(MainAnimeCount >= 85) MainAnimeCount -= 3;
                    }
                    else
                    {
                        if (OpenedCard)
                        timer1.Enabled = true; else
                        {
                            switch (whatCard)
                            {
                                case 1:
                                    sel1.isSelected = false;
                                    sel1.turnedUp = false;
                                    sel1 = null;
                                    selectionState = 0;
                                    break;
                                case 2:
                                    sel1.turnedUp = false;
                                    sel2.turnedUp = false;
                                    sel1 = sel2 = null;
                                    break;
                                case 3:
                                    sel1.inGame = sel2.inGame = false;
                                    break;
                            }
                        }
                        cards[NeedCardAn].inGame = true;
                        if(SecondCard)
                        cards[NeedCardBn].inGame = true;
                        buff_to_draw();
                        gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
                        MainAnimeCount = 0;
                        fpsForAnime = 0;
                        SecondCard = false;

                        this.MouseClick += Form1_MouseClick;
                        if (From2To1Card)
                        {
                            this.MouseClick -= Form1_MouseClick;
                            timer1.Enabled = false;
                            From2To1Card = false;
                            cards[NeedCardCn].inGame = false;
                            fpsForAnime = 2;
                            whatCard = 1;
                            sel1 = cards[NeedCardCn];
                            cards[NeedCardCn].isSelected = true;
                            cards[NeedCardCn].turnedUp = true;
                            NeedCardBn = NeedCardCn;
                            NeedCardAn = NeedCardCn;
                            WhichCardAnime = (cards[NeedCardCn].m + 1) * 5;
                            xAnime = cards[NeedCardCn].i;
                            yAnime = cards[NeedCardCn].j;
                            OpenedCard = true;
                            selectionState = 1;
                        }
                    }
                    break;
                // Menu
                case 3:
                    //Used twice to make slide eff. be faster
                    for (int i = 0; i < 2; i++)
                    {
                        gr_buffer[54].DrawImage(bmp_diam[303], 0, 0);
                        gr_buffer[101].DrawString("Credits", new Font("Showcard Gothic", 24), new SolidBrush(Color.White), PosSkullAboutUsX - 90 - SlideMoveX * SlideCount, PosSkullAboutUsY + 12);
                        // Drawing slide eff "Credits"
                        if ((PosSkullAboutUsX - 143 - SlideMoveX * SlideCount) < mouseX && (PosSkullAboutUsX - 155 + PlusMinVolX) > mouseX && (PosSkullAboutUsY - 12) < mouseY && (PosSkullAboutUsY - 20 + PlusMinVolY) > mouseY)
                        {
                            gr_buffer[53].DrawImage(bmp_diam[369], PosSkullAboutUsX - 135 - SlideMoveX * SlideCount, PosSkullAboutUsY + 12);
                            // Back for slide eff.
                            gr_buffer[51].DrawImage(bmp_diam[393], winWidth - SlideEffBackX, winHeight - SlideEffBackY);
                            gr_buffer[52].DrawImage(bmp_diam[391], PosSkullAboutUsX - 120, PosSkullAboutUsY + 12);
                            if (SlideCount <= 16)
                                SlideCount++;
                        }
                        else
                        {
                            gr_buffer[53].DrawImage(bmp_diam[368], PosSkullAboutUsX - 135 - SlideMoveX * SlideCount, PosSkullAboutUsY + 12);
                            // Back for slide eff.
                            gr_buffer[51].DrawImage(bmp_diam[393], winWidth - SlideEffBackX, winHeight - SlideEffBackY);
                            gr_buffer[52].DrawImage(bmp_diam[392], PosSkullAboutUsX - 120, PosSkullAboutUsY + 12);
                            if (SlideCount != 0)
                                SlideCount--;
                        } 
                        DrawButtonsForMenu(mouseX, mouseY, AboutUs, Loaded, Profile);
                        // Drawing music in Menu if player clicked on button "Shell"
                        if (music)
                            Music();
                        gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
                    }
                    break;
                // Drawing "Levels and Difficult" if player clicked on "Levels" in Menu
                case 4:
                    gr_buffer[54].DrawImage(bmp_diam[314], 0, 0);
                    gr_buffer[50].DrawImage(bmp_diam[335], 30, 30);
                    // Drawing button "Menu"
                    if (posMenuX < mouseX && (posMenuX + butWidth) > mouseX && (posMenuY - 4 - heightM) < mouseY && (posMenuY + butHeight -4 - heightM) > mouseY)
                          gr_buffer[40].DrawImage(bmp_diam[351], posMenuX, posMenuY);
                        else
                          gr_buffer[40].DrawImage(bmp_diam[350], posMenuX, posMenuY);

                    int CanPlay = Active.LevelIn+1;
                    int countStar = 2;
                    int count = (int)difficulty;

                    // Drawing Difficults like "EASY" and etc.
                    for(int i = 1; i < 4; i++)
                    {
                        if(count == 0)
                            gr_buffer[44 + i].DrawImage(bmp_diam[335 + 2 * i], posDESHX - 24, posDESHY + 85 * i);
                        else
                            gr_buffer[44 + i].DrawImage(bmp_diam[334 + 2*i], posDESHX, posDESHY + 85*i);
                        count--;
                    }

                    // Drawing level's "Shell" like 1, 2, 3..
                    for (int i = 1; i < 4; i++)
                        for (int j = 1; j < 4; j++)
                        {
                            if (starPosX + 180 * j < mouseX && (starPosX + starXY + 180 * j - 8) > mouseX && (starPosY + 150 * i - heightM) < mouseY && (starPosY + starXY + 150 * i - heightM) > mouseY && CanPlay > 0)
                                gr_buffer[54 + i].DrawImage(bmp_diam[315 + countStar], starPosX + 180 * j, starPosY + 150 * i);
                            else gr_buffer[54 + i].DrawImage(bmp_diam[314 + countStar], starPosX + 180 * j, starPosY + 150 * i);
                            countStar += 2;
                            CanPlay--;
                        }
                    
                    gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
                    break;
                // Drawing Score and "Stage is cleared" if player won
                case 5:
                    gr_buffer[100].DrawImage(bmp_diam[385], StgClrPosX, StgClrPosY);
                    gr_buffer[101].DrawString("Your Score: ", new Font("Showcard Gothic", 28), new SolidBrush(Color.White), StgClrPosX + 68, StgClrPosY + 54);
                    gr_buffer[102].DrawString("" + Active.Score[level-1], new Font("Showcard Gothic", 28), new SolidBrush(Color.White), StgClrPosX + 170 - 8 * moveNumber, StgClrPosY + 94);
                    if ((StgClrPosX + 120 - 4) < mouseX && (StgClrPosX + 120 + OKX - 10 - 5) > mouseX && (StgClrPosY + 150 - 32) < mouseY && (StgClrPosY + 150 + OKY - 40) > mouseY)
                        gr_buffer[103].DrawImage(bmp_diam[387], StgClrPosX + 120, StgClrPosY + 150);
                    else
                        gr_buffer[103].DrawImage(bmp_diam[386], StgClrPosX + 120, StgClrPosY + 150);
                    gr_form.DrawImageUnscaled(bmp_buffer, 0, 0);
                    break;
            }
        }

        static private void DrawButtonsForMenu(int mouseX, int mouseY, bool AboutUs, bool Loaded,bool Profile)
        {
            // Drawing 2 buttons in right down corner 
            for (int i = 0; i < 2; i++)
                if (posAbSetX + 74 * i < mouseX && (posAbSetX + XYAbSet + 80 * i - 8) > mouseX && (posAbSetY - heightM) < mouseY && (posAbSetY + XYAbSet - heightM) > mouseY)
                    gr_buffer[45 + i].DrawImage(bmp_diam[353 + i * 2], posAbSetX + 80 * i, posAbSetY);
                else
                    gr_buffer[45 + i].DrawImage(bmp_diam[352 + i * 2], posAbSetX + 80 * i, posAbSetY);
            // Drawing 5 buttons in the center like "New Game" and etc.
            for (int i = 1; i < 5; i++)
                if (xButPos < mouseX && (xButPos + butWidth - 10) > mouseX && (yButPos + 70 * i - heightM) < mouseY && (yButPos + butHeight + 70 * i - heightM) > mouseY)
                    gr_buffer[54 + i].DrawImage(bmp_diam[303 + i * 2], xButPos, yButPos + 70 * i);
                else
                    gr_buffer[54 + i].DrawImage(bmp_diam[302 + i * 2], xButPos, yButPos + 70 * i);
            // If player clicked on "Load" 
            // Drawing "OK" in window "Game is loaded"
            if (Loaded)
            {
                gr_buffer[70].DrawImage(bmp_diam[381], PosGameSLX, PosGameSLY);
                if ((PosOKX - 4) < mouseX && (PosOKX + OKX - 4) > mouseX && (PosOKY - 32) < mouseY && (PosOKY + OKY - 28) > mouseY)
                    gr_buffer[71].DrawImage(bmp_diam[383], PosOKX, PosOKY);
                else
                    gr_buffer[71].DrawImage(bmp_diam[382], PosOKX, PosOKY);
            }
            // Drawing Profile and score if player clicked on "Skull"
            if (Profile)
            {
                String ProfString = "";
                gr_buffer[72].DrawImage(bmp_diam[390], ProfPosX, ProfPosY);
                gr_buffer[101].DrawString("Profile:   " + Active.Name, new Font("Showcard Gothic", 30), new SolidBrush(Color.White), ProfPosX + 40, ProfPosY + 40);
                for (int i = 1; i <= 9; i++)
                    ProfString += "Level  " + i + " :  " + Active.Score[i - 1] + "\n";
                gr_buffer[102].DrawString(ProfString, new Font("Showcard Gothic", 30), new SolidBrush(Color.White), ProfPosX + 48, ProfPosY + 90);
            }
            if (AboutUs)
                gr_buffer[54].DrawImage(bmp_diam[400], 430, 20);
        }

        static private void setOutputVolume(int volume)
        {
            if (waveOut != null)
            {
                float outputVolume = volume / 5.0f;
                if (0.0f <= outputVolume && outputVolume <= 1.0f)
                {
                    waveOut.Volume = outputVolume;
                }
                else
                {
                    //MessageBox.Show("Can't calculate output volume.\n");
                }
            }
            else
            {
                //MessageBox.Show("Output device didn't found.\n");
            }
        }
    

}

    #region Native
    public class NativeMethods
    {

        [StructLayout(LayoutKind.Sequential)]
        public struct Message
        {
            public IntPtr hWnd;
            public uint msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool PeekMessage(out Message msg, IntPtr hWnd, uint messageFilterMin, uint messageFilterMax, uint flags);
        [DllImport("shell32.dll")]
        public static extern int ShellAbout(IntPtr hWnd, string szApp, string szOtherStuff, IntPtr hIcon);
    }
    #endregion
}

public class StatisticRecord
{
    public string name;
    public int[] score = {0,0,0,0,0,0,0,0,0};
}

public class couple_cards
{
    //public String name; // will be used for using pictures in game
    public int i,j,m;
    public bool inGame = true; // use it for checking
    public int x, y; // coordinates 
    public bool isSelected = false;
    public bool turnedUp = false;
    public String id;// id to draw a card
}

[Serializable]
public class Profile
{
    /*
     * Name     -- profile name
     * LevelIn  -- maximum level where the user was
     * Score    -- scores for each level
     */
    public string Name { get; set; }
    public int LevelIn { get; set; }
    public int[] Score { get; set; }
    public bool Anon { get; set; }

    public Profile()
    {
        Name = "Anon " + DateTime.Today;
        LevelIn = 0;
        Score = new int[(int)GDITestGame.States.nLevels];
        Anon = true;
    }

    public Profile(string name, int levelIn = 0, int[] score = null)
    {
        Name = name;
        LevelIn = levelIn;
        Score = new int[(int)GDITestGame.States.nLevels];
        if (score != null)
        {
            int i = 0;
            while (i < score.Length)
            {
                Score[i] = score[i];
                i++;
            }
        }
        Anon = false;
    }

}

